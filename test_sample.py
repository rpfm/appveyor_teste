from PyQt5.QtCore import Qt
import PyMca5.PyMcaGui.pymca as PyMca
from epics import caget, caput
#import pytest

#@pytest.fixture(scope="module")
#def application(request):

#    from PyQt5.QtWidgets import QApplication

   # request.app = QApplication([])

    #request.app.setApplicationName("py.test QApplication")



  #  def finalize():

        #request.app.deleteLater()

        #del request.app



    #request.addfinalizer(finalize)

    #return request.app
    
def test_cap():
    m2 = caput("arrobHost:xxxExample",4)
    assert m2 == 1

def test_cag():
    m1 = caget('arrobHost:xxxExample')
    assert m1 == 4

def func(x):
    return x + 1


def test_answer():
    assert func(4) == 5



class TestClass(object):


    def test_one(self):
        x = "this"
        assert "h" in x


    def test_two(self):
        x = "hello"
        assert hasattr(x, "hello")
