import QtQuick 2.6
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2

import CSDataQuick.Data 1.0
import CSDataQuick.Components 1.0


Item {
    id: main
    property alias cslabelobj: cstext
    CSTextUpdate {
        id: cstext
        source: 'arrobHost:xxxExample'
        colorMode: ColorMode.Alarm
        background: "gray"
        anchors.fill: parent    
    }
}